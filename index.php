<?php

require_once 'lib/tpl.php';
require_once 'Book.php';




//Front Controller
$cmd = param('cmd') ? param('cmd') : 'nimekiri';
$data = [];
function param($key) {
    if (isset($_GET[$key])) {
        return $_GET[$key];
    } else if (isset($_POST[$key])) {
        return $_POST[$key];
    } else {
        return '';
    }
}
if ($cmd === 'nimekiri') {
    $data['$title'] = 'Nimekiri';
    $data['$template'] = 'datalist.html';
    $data['$book'] = getBook();
    print render_template('mainpage.html', $data);

} else if ($cmd === 'lisa') {
    $data['$title'] = 'Lisa';
    $data['$template'] = 'form.html';
    $data['$cmd'] = 'add_data';
    print render_template('mainpage.html', $data);
} else if ($cmd === 'add_data') {
    $data['$title'] = 'Nimekiri';
    $name = param('name');
    $sectors = param('sectors');
    $agreed = param('agreed');
    add_data_to_file($name,$sectors, $agreed);
    header("Location: ?cmd=nimekiri");

} else {
    throw new Error('programming error');
}

//Process file and generate objects and add to list

function getBook()
{
    // get Book from database;

    $connection = new PDO('sqlite:data1.sqlite');
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $table = $connection-> prepare('SELECT * FROM contacts');
    $table->execute();

    foreach ($table as $row) {

        $name = $row['name'];
        $sectors = $row['sectors'];
        $agreed = $row['agreed'];

        $book[] = new Book($name, $sectors, $agreed);
    }

    return $book;
}

function add_data_to_file($name, $sectors, $terms) {

    //add data to database;
    $connection = new PDO('sqlite:data1.sqlite');
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //insert names to table;
    $stmt1 = $connection-> prepare('insert into contacts (name, sectors, agreed) VALUES (:first, :sectors, :terms)');

    $stmt1->bindParam(':first', $name);
    $stmt1->bindParam( ':sectors', $sectors);
    $stmt1->bindParam( ':terms', $terms);
    $stmt1->execute();


}







